var portal_url = "/sites/nqjzde_draft/";
var c_data = c_data || {};
c_data.animations = [];
c_data.comps = [];
c_data.timelines = [
    {
        iType: 0,
        isNew: true,
        animations: [
            "ani_0de0bd1d82c3e338",
            "ani_951e38bab5a7ad79",
            "ani_71d2a33abdabe84a",
            "ani_e5a6cf94fc701fba",
            "ani_2c3fe85e75df9da1"
        ],
        element_id: "body_d9c5d4d472848d41",
        data: { type: 0, t: { de: 0, rp: -1, name: "", st: 2 }, d: {} },
        id: "M_7cdf39fd27785094"
    },
    {
        iType: 0,
        isNew: true,
        animations: [],
        element_id: "section_dd3a1d8f",
        data: {
            type: 0,
            t: { rv: 0, rp: 0, wa: 0, de: 0, st: 1, du: 1, es: 0 },
            d: {}
        },
        id: "M_995342a3062c4ca0"
    },
    {
        iType: 0,
        isNew: true,
        animations: [],
        element_id: "section_8751193e",
        data: {
            type: 0,
            t: { rv: 0, rp: 0, wa: 0, de: 0, st: 1, du: 1, es: 0 },
            d: {}
        },
        id: "M_5e9db67a2cea63ff"
    },
    {
        iType: 0,
        isNew: true,
        animations: [],
        element_id: "section_4845c897",
        data: {
            type: 0,
            t: { rv: 0, rp: 0, wa: 0, de: 0, st: 1, du: 1, es: 0 },
            d: {}
        },
        id: "M_febbdf07e501f3c0"
    },
    {
        iType: 0,
        isNew: true,
        animations: [],
        element_id: "section_dae59a0e",
        data: {
            type: 0,
            t: { rv: 0, rp: 0, wa: 0, de: 0, st: 1, du: 1, es: 0 },
            d: {}
        },
        id: "M_64ea0387d40bb356"
    },
    {
        iType: 0,
        isNew: true,
        animations: [],
        element_id: "section_a246375d",
        data: {
            type: 0,
            t: { rv: 0, rp: 0, wa: 0, de: 0, st: 1, du: 1, es: 0 },
            d: {}
        },
        id: "M_f28a8c9ac72ee8c8"
    }
];
c_data.actions = [
    {
        element_id: "heading_0f9c3da9",
        data: { args: { a_ids: [], e_ids: [], st: 1 }, type: 0, exec: 0 },
        id: "M_c6c5c3c6d02da70f",
        isNew: true
    },
    {
        element_id: "headinga673e30d",
        data: { args: { e_ids: [], st: 1, a_ids: [] }, type: 0, exec: 0 },
        id: "act_df0eae8d",
        isNew: true
    },
    {
        element_id: "heading599ff967",
        data: { args: { e_ids: [], st: 1, a_ids: [] }, type: 0, exec: 0 },
        id: "act_6f4ec8f3",
        isNew: true
    },
    {
        element_id: "headinge51eaa68",
        data: { args: { e_ids: [], st: 1, a_ids: [] }, type: 0, exec: 0 },
        id: "act_fe0dec07",
        isNew: true
    }
];
