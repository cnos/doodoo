const custom = doodoo.bookshelf.Model.extend({
    tableName: "custom",
    hasTimestamps: true,
    hidden: ["password"]
});

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "wx_user",
    hasTimestamps: true,
    custom: function() {
        return this.belongsTo(custom, "custom_id");
    }
});
