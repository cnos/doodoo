const moment = require("moment");
const base = require("./../../base");

module.exports = class extends base {
    async _initialize() {
        await super.isCustomAuth();
        await super.isAppAuth();
        await super.isShopAuth();
    }

    async index() {
        const shopId = this.state.shop.id;
        const shop = await this.model("label")
            .query(qb => {
                qb.where("shop_id", shopId);
            })
            .fetchAll();
        this.success(shop);
    }

    async add() {
        const shopId = this.state.shop.id;
        const { id, name, info } = this.post;
        const label = await this.model("label")
            .forge({
                id,
                shop_id: shopId,
                name,
                info
            })
            .save();
        this.success(label);
    }

    async info() {
        const { id } = this.query;
        const shopId = this.state.shop.id;
        const label = await this.model("label")
            .query(qb => {
                qb.where("id", id);
                qb.where("shop_id", shopId);
            })
            .fetch();
        this.success(label);
    }

    async del() {
        const { id } = this.query;
        const shopId = this.state.shop.id;
        const label = await this.model("label")
            .query(qb => {
                qb.where("id", id);
            })
            .destroy();
        this.success(label);
    }
};
