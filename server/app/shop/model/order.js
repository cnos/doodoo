const order_detail = require("./order_detail");
const order_contact = require("./order_contact");
const service = require("./order_service");
const verify = require("./order_verify");
const kuaidi = require("./order_kuaidi");
const stores = require("./stores");
const user = doodoo.bookshelf.Model.extend({
    tableName: "user",
    hasTimestamps: true
});

module.exports = doodoo.bookshelf.Model.extend({
    tableName: "shop_order",
    hasTimestamps: true,
    detail: function() {
        return this.hasMany(order_detail, "order_id");
    },
    contact: function() {
        return this.hasOne(order_contact, "order_id");
    },
    user: function() {
        return this.belongsTo(user, "user_id");
    },
    service: function() {
        return this.hasMany(service, "order_id");
    },
    verify: function() {
        return this.hasOne(verify, "order_id");
    },
    kuaidi: function() {
        return this.hasOne(kuaidi, "order_id");
    },
    store: function() {
        return this.belongsTo(stores, "store_id");
    }
});
